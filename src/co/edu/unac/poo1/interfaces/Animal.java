package co.edu.unac.poo1.interfaces;
// interface
interface Animal {
    // A interface only has abstract methods
    public void animalSound(); // interface method (does not have a body)
    public void run(); // interface method (does not have a body)
}
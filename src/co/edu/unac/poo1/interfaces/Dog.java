package co.edu.unac.poo1.interfaces;

public class Dog implements Animal{

    @Override
    public void animalSound() {
        System.out.println("Guau!");
    }

    @Override
    public void run() {
        System.out.println("Run!");
    }
}

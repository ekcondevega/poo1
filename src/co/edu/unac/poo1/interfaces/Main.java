package co.edu.unac.poo1.interfaces;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.animalSound();
        dog.run();
    }
}

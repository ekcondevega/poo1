package co.edu.unac.poo1.fundamentals;

import java.util.Scanner;

public class Operators {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Este programa determina la relación que existe entre dos números");
        System.out.println("Ingrese el primer número");
        int a = input.nextInt();
        System.out.println("Ingrese el segundo número");
        int b = input.nextInt();
        if (a > b) {
            System.out.println(a + " es mayor que " + b);
        } else {
            System.out.println(a + " es menor que " + b);
        }

        if (a >= b) {
            System.out.println(a + " es mayor o igual que " + b);
        } else {
            System.out.println(a + " es menor o igual que " + b);
        }

        if (a == b) {
            System.out.println(a + " es igual a " + b);
        } else {
            System.out.println(a + " es diferente a " + b);
        }

    }
}
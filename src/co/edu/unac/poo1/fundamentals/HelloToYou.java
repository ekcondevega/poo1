package co.edu.unac.poo1.fundamentals;

import java.util.Scanner;

//Este programa te saluda con tu nombre

public class HelloToYou {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Por favor ingresa tu nombre");
        String name = input.nextLine();

        System.out.println(":)\t\"¡Hola!\t" + name + "\"");

    }

}
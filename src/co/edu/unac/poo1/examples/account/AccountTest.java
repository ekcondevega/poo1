package co.edu.unac.poo1.examples.account;

import java.util.Scanner;

public class AccountTest {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Account account1 = new Account("Noah Blue", 100.00);
        Account account2 = new Account("Toby Green", -100.00);

        int option =2;
        while(option != 0) {
            if (option == 1) {
                System.out.println("Ingrese la cantidad a depositar en la cuenta 1");
                account1.deposit(sc.nextDouble());
                System.out.println("Ingrese la cantidad a depositar en la cuenta 2");
                account2.deposit(sc.nextDouble());
            }
            if (option == 2) {
                System.out.println(account1.toString());
                System.out.println(account2.toString());
            }

            System.out.println("Seleccione el movimiento que desea realizar\n" +
                    "1. Depositar dinero\n2. Ver el estado de " +
                    "cuenta\n0. Salir");
            option = sc.nextInt();
        }

    }



}

package co.edu.unac.poo1.examples.account;

public class Account {

    private String name;
    private double balance;

    //Constructor por defecto
    public Account() {

    }

    //Constructor con parámetros
    public Account(String name, double balance) {
        this.name = name;
        if (balance > 0.0) {
            this.balance = balance;
        } else {
            this.balance = 0.0;
        }
    }

    //Getters y setters

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        if (balance > 0.0) {
            this.balance = balance;
        } else {
            this.balance = 0.0;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Otros métodos
    public void deposit(double depositAmount) {
        if (depositAmount > 0.0) {
            balance = balance + depositAmount;
        }
    }

    //Método toString() para mostrar la información del objeto
    @Override
    public String toString() {
        return "Account{" +
                "name ='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}

package co.edu.unac.poo1.examples.person;

public class Persona {
    private String nombre;

    //constructor por defecto
    public Persona() {

    }

    //constructor con parámetros
    public Persona(String nombre) {

        this.nombre = nombre;
    }

    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    public String getNombre() {

        return nombre;
    }


    public String saludar() {

        return nombre;
    }

    @Override
    public String toString() {
        return "Person{" +
                "nombre='" + nombre + '\'' +
                '}';
    }
}

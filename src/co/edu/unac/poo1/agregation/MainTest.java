package co.edu.unac.poo1.agregation;

import java.util.ArrayList;

public class MainTest {
    public static void main(String[] args) {
        Student student1 = new Student("Katherine", 1, "Ingeniería");
        Student student2 = new Student("Adriana", 2, "Ingeniería");
        ArrayList<Student> students = new ArrayList<Student>();
        students.add(student1);
        students.add(student2);
        Faculty faculty1 = new Faculty("Ingeniería", students);
        System.out.println(faculty1.toString());

    }
}

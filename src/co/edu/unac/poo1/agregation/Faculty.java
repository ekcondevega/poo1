package co.edu.unac.poo1.agregation;

import java.util.ArrayList;

public class Faculty {

    private String name;
    private ArrayList<Student> students;

    public Faculty() {

    }

    public Faculty(String name, ArrayList<Student> students)
    {

        this.name = name;
        this.students = students;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "name='" + name + '\'' +
                ", students=" + students +
                '}';
    }
}

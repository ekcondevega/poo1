package co.edu.unac.poo1.inheritance.example2;

public class Documental extends ContenidoMultimedia {

    //Atributos únicos de la clase Documental
    private String region;

    //Constructor por defecto
    public Documental() {
    }

    //Constructor con todos los parámetros
    public Documental(String nombre, String tipo, int duracion, String genero, int cantidadVistas, String region) {
        super(nombre, tipo, duracion, genero, cantidadVistas);
        this.region = region;
    }

    //Métodos get y set

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    //Método toString() modificado para película
    @Override
    public String toString() {
        return "Documental{ nombre='" + getNombre() + '\'' +
                ", tipo='" + getTipo() + '\'' +
                ", duracion=" + getDuracion() +
                ", genero='" + getGenero() + '\'' +
                ", cantidadVistas=" + getCantidadVistas()+
                "region='" + region + '\'' +
                '}';
    }
}

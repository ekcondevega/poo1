package co.edu.unac.poo1.inheritance.example2;

public class ContenidoMultimedia {

    //Atributos
    private String nombre;
    private String tipo;
    private int duracion;
    private String genero;
    private int cantidadVistas;

    //Constructor por defecto
    public ContenidoMultimedia() {

    }

    //Constructor con todos los parámetros
    public ContenidoMultimedia(String nombre, String tipo, int duracion, String genero, int cantidadVistas) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.duracion = duracion;
        this.genero = genero;
        this.cantidadVistas = cantidadVistas;
    }


    //Métodos get y set

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getCantidadVistas() {
        return cantidadVistas;
    }

    public void setCantidadVistas(int cantidadVistas) {
        this.cantidadVistas = cantidadVistas;
    }

    //Método toString()
    @Override
    public String toString() {
        return "ContenidoMultimedia{" +
                "nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", duracion=" + duracion +
                ", genero='" + genero + '\'' +
                ", cantidadVistas=" + cantidadVistas +
                '}';
    }

    //Otros métodos
    public void verContenido(int numeroVistas) {
        cantidadVistas += numeroVistas;
    }

}

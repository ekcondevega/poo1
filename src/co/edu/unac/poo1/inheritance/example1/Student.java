package co.edu.unac.poo1.inheritance.example1;

public class Student extends Person {

    //Definir el constructor para Student con los atributos de Person
    //El constructor tendrá atributos únicos de Student

    //Atributo único de Student
    private int semester;
    private int idStudent;

    //Crear el constructor
    //Constructor por defecto
    public Student() {

    }

    //Constructor con todos los parámetros
    public Student(String firstName, String lastName, int age, int semester, int idStudent) {
        //El super llama a mi superclase (o clase padre) para guardar los valores
        super(firstName, lastName, age);
        this.semester = semester;
        this.idStudent = idStudent;
    }

    //Getters y setters

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }


    //toString()
    @Override
    public String toString() {
        return "Estudiante -> nombre: " + getFirstName() + ", apellido: " + getLastName() +
                ", edad:" + getAge() + ", semestre: " + semester + ", id: " + idStudent;
    }
}

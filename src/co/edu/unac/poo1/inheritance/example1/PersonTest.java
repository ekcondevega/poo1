package co.edu.unac.poo1.inheritance.example1;

import java.util.Scanner;

public class PersonTest {

    public static void main(String[] args) {

        //Clase objeto = new Clase(parámetros);
        Person person1 = new Person("Katherine", "Conde", 18);
        System.out.println(person1);

        //Clase objeto = new Clase(); -> constructor por defecto, no tiene parámetros
        //Para añadir atributos yo debo usar el metodo set()
        Person person2 = new Person();
        Scanner scanner = new Scanner(System.in);
        person2.setFirstName(scanner.nextLine());
        person2.setLastName(scanner.nextLine());
        person2.setAge(scanner.nextInt());
        System.out.println(person2);

        //Clase objeto = new Clase();
        //Vamos a crear un estudiante que hereda de la clase Persona
        Student student1 = new Student("Katherine", "Conde", 20, 1, 2022123);
        System.out.println(student1.toString());
    }
}

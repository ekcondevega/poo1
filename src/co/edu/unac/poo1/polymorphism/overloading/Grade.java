package co.edu.unac.poo1.polymorphism.overloading;

import java.util.Arrays;

public class Grade {

    private String courseName = "";
    private double[] grades;

    public Grade() {

    }

    public Grade(String courseName, double[] grades) {
        this.courseName = courseName;
        this.grades = grades;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public double[] getGrades() {
        return grades;
    }

    public void setGrades(double[] grades) {
        this.grades = grades;
    }

    public double sum() {
        double varSum = 0;
        for (double item: grades) {
            varSum += item;
        }
        return varSum;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "courseName='" + courseName + '\'' +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}

package co.edu.unac.poo1.polymorphism.overloading;

public class Message {

    private String subject = "";
    private String message = "";
    private String emailRecipient = "";

    public Message() {

    }

    public Message(String subject, String message, String emailRecipient) {
        this.subject = subject;
        this.message = message;
        this.emailRecipient = emailRecipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmailRecipient() {
        return emailRecipient;
    }

    public void setEmailRecipient(String emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public String sum() {
        String varSum = "";
        varSum = "To: " + emailRecipient + "\nAsunto: " + subject + "\nMensaje: " + message;
        return varSum;
    }

    @Override
    public String toString() {
        return "Message{" +
                "subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", emailRecipient='" + emailRecipient + '\'' +
                '}';
    }
}

package co.edu.unac.poo1.polymorphism.overloading;

public class Main {

    public static void main(String[] args) {
        Grade grade = new Grade("POO1", new double[] {4.5, 5, 4.2});
        Message message = new Message("Hello!", "Nice to meet you", "correo@gmail.com");
        System.out.println("Sum in Grade\n" + grade.sum());
        System.out.println("-----------------------------------------------------------");
        System.out.println("Sum in Message\n" + message.sum());
    }

}

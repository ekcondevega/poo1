package co.edu.unac.poo1.polymorphism.inclusion;

public class Square extends Shape {

    private double sideValue = 0.0;

    // Constructor por defecto
    public Square() {

    }

    // Constructor con el nuevo parámetro
    public Square(double sideValueParam) {
        super("Cuadrado");
        this.sideValue = sideValueParam;
    }

    @Override
    public double getArea() {
        return this.sideValue * this.sideValue;
    }

    @Override
    public double getPerimeter() {
        return this.sideValue * 4;
    }

    @Override
    public String toString() {
        return super.toString() + ", sideValue=" + sideValue +
                ", area=" + getArea() +
                ", perimeter=" + getPerimeter();
    }
}

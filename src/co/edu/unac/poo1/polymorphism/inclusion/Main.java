package co.edu.unac.poo1.polymorphism.inclusion;

import org.w3c.dom.ls.LSOutput;

public class Main {

    public static void main(String[] args) {

        Shape shape = new Shape("Cuadrado");
        System.out.println(shape.getArea());
        System.out.println(shape);

        Square square = new Square(2);
        square.setSidesNumber(4);
        System.out.println(square.getArea());
        System.out.println(square);

        // Polimorfismo de inclusión (con herencia)
        // Creo un objeto de tipo Shape que se crea
        // como un cuadrado
        Shape shapeSquare = new Square(10);
        shapeSquare.setSidesNumber(4);
        System.out.println(shapeSquare.getArea());
        System.out.println(shapeSquare);
    }


}

package co.edu.unac.poo1.polymorphism.inclusion;

public class Shape {

    private int sidesNumber = 0;
    private String name = "";
    double area = 0.0;
    double perimeter = 0.0;

    // Constructor por defecto
    public Shape() {

    }

    // Constructor con 1 parámetro: name
    public Shape(String name) {
        this.name = name;
    }

    public Shape(String name, int sidesNumber) {
        this.name = name;
        this.sidesNumber = sidesNumber;
    }

    public Shape(int sidesNumber) {
        this.sidesNumber = sidesNumber;
    }

    // Constructor con todos los parámetros
    public Shape(int sidesNumber, String name, double area, double perimeter) {
        this.sidesNumber = sidesNumber;
        this.name = name;
        this.area = area;
        this.perimeter = perimeter;
    }

    public int getSidesNumber() {
        return sidesNumber;
    }

    public void setSidesNumber(int sidesNumber) {
        this.sidesNumber = sidesNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    @Override
    public String toString() {
        return "Shape " +
                "sidesNumber=" + sidesNumber +
                ", name='" + name;
    }
}

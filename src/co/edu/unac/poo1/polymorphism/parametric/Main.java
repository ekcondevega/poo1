package co.edu.unac.poo1.polymorphism.parametric;

public class Main {

    public static void main(String[] args) {
        Car car = new Car("Mazda");
        System.out.println(car.carModel());
        System.out.println(car.carModel(2023));
        System.out.println(car.carModel(2023, true));
    }

}

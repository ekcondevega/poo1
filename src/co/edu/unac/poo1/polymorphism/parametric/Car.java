package co.edu.unac.poo1.polymorphism.parametric;

public class Car {

    private String brand = "";
    private int year = 0;
    private String name = "";
    private boolean is4x4 = false;

    // Constructor por defecto
    public Car() {

    }

    // Constructor con 1 parámetro
    public Car(String brand) {
        this.brand = brand;
    }

    // Constructor con 1 parámetro
    public Car(int year) {
        this.year = year;
    }

    // Constructor con 1 parámetro
    public Car(boolean is4x4) {
        this.is4x4 = is4x4;
    }

    // Constructor con 2 parámetros
    public Car(String brand, int year) {
        this.brand = brand;
        this.year = year;
    }

    // Constructor con 3 parámetros
    public Car(String brand, int year, String name) {
        this.brand = brand;
        this.year = year;
        this.name = name;
    }

    // Constructor con todos los parámetros
    public Car(String brand, int year, String name, boolean is4x4) {
        this.brand = brand;
        this.year = year;
        this.name = name;
        this.is4x4 = is4x4;
    }

    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public boolean isIs4x4() {
        return is4x4;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIs4x4(boolean is4x4) {
        this.is4x4 = is4x4;
    }

    // POLIMORFISMO PARAMÉTRICO
    public String carModel() {
        return "El carro es un " + brand + " y no tiene modelo";
    }
    public String carModel(int yearParam) {
        return "El carro es un " + brand + " modelo " + yearParam;
    }

    public String carModel(int yearParam, boolean is4x4Param) {
        return "El carro es un " + brand + " modelo " + yearParam
                + " y 4X4 = " + is4x4Param ;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", year=" + year +
                ", name='" + name + '\'' +
                ", is4x4=" + is4x4 +
                '}';
    }
}

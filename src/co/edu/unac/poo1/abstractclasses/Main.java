package co.edu.unac.poo1.abstractclasses;

public class Main {

    public static void main(String[] args) {

        Circle circle = new Circle("Circulo", 1, "Azul", 2);
        System.out.println(circle);
        System.out.println("Area = " + circle.area());
        System.out.println("Perímetro = " + circle.perimeter());



    }

}

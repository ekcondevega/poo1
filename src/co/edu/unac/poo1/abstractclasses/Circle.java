package co.edu.unac.poo1.abstractclasses;

public class Circle extends TwoDimensionalShape {

    private double radius = 0.0;

    private double diameter = 0.0;

    public Circle() {

    }

    public Circle(String name, int sidesNumber, String color, double radius) {
        super(name, sidesNumber, color);
        this.radius = radius;
        this.diameter = radius * 2;
    }

    public Circle(String name, int sidesNumber, String color, double radius, double diameter) {
        super(name, sidesNumber, color);
        this.radius = radius;
        this.diameter = diameter;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return super.toString() + " radius=" + radius +
                ", diameter=" + diameter +
                '.';
    }

    @Override
    public double area() {
        return Math.PI * (this.radius * this.radius);
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * this.radius;
    }
}

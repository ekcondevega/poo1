package co.edu.unac.poo1.abstractclasses;

public abstract class TwoDimensionalShape {

    private String name = "";
    private int sidesNumber = 0;

    private String color = "";

    public TwoDimensionalShape() {

    }

    public TwoDimensionalShape(String name, int sidesNumber, String color) {
        this.name = name;
        this.sidesNumber = sidesNumber;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSidesNumber() {
        return sidesNumber;
    }

    public void setSidesNumber(int sidesNumber) {
        this.sidesNumber = sidesNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Two Dimensional Shape: " +
                "name='" + name + '\'' +
                ", sidesNumber=" + sidesNumber +
                ", color='" + color + ", ";
    }

    // abstract method must be overridden by concrete subclasses
    public abstract double area(); // no implementation here

    // abstract method must be overridden by concrete subclasses
    public abstract double perimeter(); // no implementation here
}

package co.edu.unac.poo1.collections;

import java.util.Arrays;

public class DataStructures {
    public static void main(String[] args) {
        int[] array = new int[1];
        System.out.println(array);
        array[0] = 12;
        System.out.println(array[0]);

        int[] array2 = {1,2,3};
        System.out.println(array2[0]);

        try
        {
            array2[10] = 2;
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            System.out.println(e);
            System.out.println("Intenta nuevamente");
        }

        for (int counter = 0; counter < array2.length; counter++)
            System.out.print(array2[counter] + " ");

        System.out.println("\n-----------------------------------");

        for (int number : array2)
            System.out.print(number + " ");


        System.out.println("\n-----------------");
        int[][] b_array = {{1, 2}, {3, 4}};
        System.out.println(b_array[0][0]);

        System.out.println("\n-----------------");
        int[][] b_array2 = new int[2][]; // Array con dos filas
        b_array2[0] = new int[5]; // Crear 5 columnas para la fila 1
        b_array2[1] = new int[3]; // Crear 3 columnas para la fila 2
        b_array2[0][0] = 1;


        int[][] m_array = {{1,2,3,4,5}, {1,2,3}};

        for (int row = 0; row < m_array.length; row++)
        {
            for (int column = 0; column < m_array[row].length; column++)
                System.out.print(m_array[row][column] + " ");
            System.out.println();
        }

        System.out.println("-------------------------");
        double[] doubleArray = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        Arrays.sort(doubleArray);



    }
}

package co.edu.unac.poo1.collections.generics;

public class GenericsMethodsInClass {
    public static void main(String[] args) {
        Integer[] integerArray = {1, 2, 3, 4, 5, 6};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
        Character[] characterArray = {'H', 'E', 'L', 'L', 'O'};
        printIntegerArray(integerArray);
        printDoubleArray(doubleArray);
        printCharacterArray(characterArray);
        printArray(integerArray);
        printArray(doubleArray);
        printArray(characterArray);
    }

    // Método para imprimir el arreglo de Integers
    public static void printIntegerArray(Integer[] inputArray) {
        for (Integer item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    // Método para imprimir el arreglo de Doubles
    public static void printDoubleArray(Double[] inputArray) {
        for (Double item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    // Método para imprimir el arreglo de Characters
    public static void printCharacterArray(Character[] inputArray) {
        for (Character item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    // Método genérico
    public static <T> void printArray(T[] inputArray) {
        for (T item: inputArray) { // for (T*Tipo de dato* item*Nombre de la variable con la que itero*
                                    // : inputArray*Nombre del arreglo, array o lista)
            System.out.print(item + " ");
        }
        System.out.println();
    }


}

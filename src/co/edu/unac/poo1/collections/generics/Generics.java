package co.edu.unac.poo1.collections.generics;

import co.edu.unac.poo1.collections.exercise.city.Ciudad;

import java.util.ArrayList;

public class Generics {

    public static void main(String[] args) {
        /*
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        ArrayList<Ciudad> ciudades = new ArrayList<Ciudad>();
        ArrayList<String> arayListString = new ArrayList<String>();
        */

        ArrayList<String> words = new ArrayList<String>();
        words.add("blue");
        words.add("pink");
        words.add("purple");
        for (String item: words) {
            System.out.println(item);
        }
        System.out.println();
        System.out.println("Eliminamos el elemento de la posición 0");
        words.remove(0);
        System.out.println("Obtenemos el elemento de la posición 0 = " + words.get(0));
        System.out.println();
        System.out.println("La longitud del ArrayList es " + words.size());
        for (String item: words) {
            System.out.println(item);
        }
        System.out.println();
        System.out.println("¿El ArrayList contiene \"blue\"? = " + words.contains("blue"));

    }
}

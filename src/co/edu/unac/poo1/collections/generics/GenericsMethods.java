package co.edu.unac.poo1.collections.generics;

public class GenericsMethods {
    public static void main(String[] args) {
        Integer[] integerArray = {1, 2, 3, 4, 5, 6};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7};
        Character[] characterArray = {'H', 'E', 'L', 'L', 'O'};

        System.out.println("integerArray");
        printIntegerArray(integerArray);

        System.out.println("doubleArray");
        printDoubleArray(doubleArray);

        System.out.println("characterArray");
        printCharacterArray(characterArray);

        System.out.println("integerArray");
        printArray(integerArray);

        System.out.println("doubleArray");
        printArray(doubleArray);

        System.out.println("characterArray");
        printArray(characterArray);

        printFirstLastElement(integerArray);
        printFirstLastElement(doubleArray);
        printFirstLastElement(characterArray);

        printOdd(integerArray);
        printOdd(doubleArray);
        printOdd(characterArray);

        printEven(integerArray);
        printEven(doubleArray);
        printEven(characterArray);

    // Llave de cierre del psvm()
    }

    public static void printIntegerArray(Integer[] inputArray) {
        for (Integer item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public static void printDoubleArray(Double[] inputArray) {
        for (Double item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public static void printCharacterArray(Character[] inputArray) {
        for (Character item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public static <T> void printArray(T[] inputArray) {
        for (T item: inputArray) {
            System.out.print(item + " ");
        }
        System.out.println();
    }

    public static <T> void printFirstLastElement(T[] inputArray) {
        System.out.println("El primer elemento del array es " + inputArray[0]);
        System.out.println("El primer elemento del array es " + inputArray[inputArray.length-1]);
    }

    // Posiciones impares
    public static <T> void printOdd(T[] inputArray) {
        int i = 0;
        for (T item: inputArray) {
            if (i % 2 != 0) {
                System.out.print(item + " ");
            }
            i++;
        }
        System.out.println();
    }

    // Posiciones pares
    public static <T> void printEven(T[] inputArray) {
        int i = 0;
        for (T item: inputArray) {
            if (i % 2 == 0) {
                System.out.print(item + " ");
            }
            i++;
        }
        System.out.println();
    }


}

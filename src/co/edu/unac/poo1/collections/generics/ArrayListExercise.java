package co.edu.unac.poo1.collections.generics;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListExercise {
    public static void main(String[] args) {
        ArrayList<Double> notas = new ArrayList<Double>();
        notas.add(4.2);
        notas.add(5.0);
        notas.add(4.5);
        notas.remove(2);
        System.out.println(notas.size());
    }
}

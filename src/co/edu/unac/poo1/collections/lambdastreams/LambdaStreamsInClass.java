package co.edu.unac.poo1.collections.lambdastreams;

import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class LambdaStreamsInClass {
    public static void main(String[] args) {
        // Array que voy a utilizar con Streams y lambdas
        int[] values = {3, 10, 6, 1, 4, 8, 2, 5, 9, 7};
        // Imprimir los valores originales
        System.out.print("Original values: ");
        /* IntStream -> Tipo de dato al que convierto el arreglo - Colección de enteros
        IntStream.of(values) -> Convertir a IntStream unos valores, esos valores son "values"
        "values" es la lista de enteros
        forEach -> método del Stream para iterar cada elemento
        value -> System.out.printf("%d ", value) *** Esto es lambda, con 1 parámetro y 1 sentencia
         */
        IntStream.of(values).forEach(value -> System.out.printf("%d ", value));

        System.out.println();

        /*
        %n En printf significa que añado una nueva línea
        %d Indica que se imprimirá un valor decimal

        printf("%nCount: %d%n", IntStream.of(values).count())
        Esto de arriba indica que el printf recibe 2 parámetros
        Parámetro 1: Cómo se va a mostrar el texto
        Parámetro 2: El valor que va a mostrar
         */
        System.out.printf("%nCount: %d%n", IntStream.of(values).count());
        System.out.printf("Min: %d%n", IntStream.of(values).min().getAsInt());
        // getAsInt() -> obtener como entero
        System.out.printf("Max: %d%n", IntStream.of(values).max().getAsInt());
        System.out.printf("Sum: %d%n", IntStream.of(values).sum());
        System.out.printf("Average: %.2f%n", IntStream.of(values).average().getAsDouble());
        System.out.printf("%nEven values displayed in sorted order: ");
        IntStream.of(values).filter(value -> value % 2 == 0).sorted().forEach(value -> System.out.printf("%d ", value));
        System.out.printf("%nOdd values displayed in sorted order: ");
        IntStream.of(values).filter(value -> value % 2 != 0).sorted().forEach(value -> System.out.printf("%d ", value));
        System.out.println("\n");

    }
}

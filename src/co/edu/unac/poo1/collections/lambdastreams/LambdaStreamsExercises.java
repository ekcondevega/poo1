package co.edu.unac.poo1.collections.lambdastreams;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class LambdaStreamsExercises {

    public static void main(String[] args) {

        int[] values = {3, 10, 6, 1, 4, 8, 2, 5, 9, 7};
        System.out.print("Original values: ");
        IntStream.of(values).forEach(value -> System.out.printf("%d ", value));
        System.out.println();
        System.out.printf("%nCount: %d%n", IntStream.of(values).count());
        System.out.printf("Min: %d%n", IntStream.of(values).min().getAsInt());
        System.out.printf("Max: %d%n", IntStream.of(values).max().getAsInt());
        System.out.printf("Sum: %d%n", IntStream.of(values).sum());
        System.out.printf("Average: %.2f%n", IntStream.of(values).average().getAsDouble());
        System.out.printf("%nEven values displayed in sorted order: ");
        IntStream.of(values).filter(value -> value % 2 == 0).sorted().forEach(value -> System.out.printf("%d ", value));
        System.out.printf("%nOdd values displayed in sorted order: ");
        IntStream.of(values).filter(value -> value % 2 != 0).sorted().forEach(value -> System.out.printf("%d ", value));
        System.out.println("\n");

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);

        System.out.print("Elementos del arrayList: ");
        arrayList.forEach(n -> System.out.printf("%d ", n));
        System.out.println("\n");

        System.out.print("Elementos del arrayList con clasificación de par o impar: \n");
        arrayList.forEach(n -> {
            if (n % 2 == 0) {
                System.out.print("Par: ");
                System.out.printf("%d ", n);
                System.out.println();
            } else {
                System.out.print("Impar: ");
                System.out.printf("%d ", n);
                System.out.println();
            }
        });


    }

}

package co.edu.unac.poo1.collections.lambdastreams;

import java.util.ArrayList;
import java.util.stream.DoubleStream;

public class LambdaStreamsExerciseDouble {
    public static void main(String[] args) {
        double[] values = {4.5, 4.2, 5, 4.9, 3.5, 3.2, 4, 3, 2, 3.8};
        System.out.print("Original values: ");
        DoubleStream.of(values).forEach(value -> System.out.printf("%.1f ", value));
        System.out.println();
        System.out.printf("%nCount: %d%n", DoubleStream.of(values).count());
        System.out.printf("Min: %.1f%n", DoubleStream.of(values).min().getAsDouble());
        System.out.printf("Max: %.1f%n", DoubleStream.of(values).max().getAsDouble());
        System.out.printf("Sum: %.1f%n", DoubleStream.of(values).sum());
        System.out.printf("Average: %.2f%n", DoubleStream.of(values).average().getAsDouble());
        System.out.printf("%nEven values displayed in sorted order: ");
        DoubleStream.of(values).filter(value -> value % 2 == 0).sorted().forEach(value -> System.out.printf("%.1f ", value));
        System.out.printf("%nOdd values displayed in sorted order: ");
        DoubleStream.of(values).filter(value -> value % 2 != 0).sorted().forEach(value -> System.out.printf("%.1f ", value));
        System.out.println("\n");

        ArrayList<Double> arrayList = new ArrayList<Double>();
        arrayList.add(4.5);
        arrayList.add(2.6);
        arrayList.add(3.0);
        arrayList.add(4.5);
        arrayList.add(5.0);

        System.out.print("Elementos del arrayList: ");
        arrayList.forEach(n -> System.out.printf("%.1f ", n));
        System.out.println("\n");

        System.out.print("Elementos del arrayList con clasificación de par o impar: \n");
        arrayList.forEach(n -> {
            if (n > 3.0) {
                System.out.print("Aprobado: ");
                System.out.printf("%.1f ", n);
                System.out.println();
            } else {
                System.out.print("Reprobado: ");
                System.out.printf("%.1f ", n);
                System.out.println();
            }
        });
    }
}

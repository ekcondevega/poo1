package co.edu.unac.poo1.collections.bidimensionalarray;

import java.util.Arrays;
import java.util.Scanner;

public class BidimensionalArray {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Definir un array inicializado con los datos
        int[][] biArray = {{1, 2}, {3, 4}};

        // Imprimir el array --> Imprime el hashcode del array
        /*
        A hash code is a numeric value that is used to identify an object during equality testing.
        [[I@4c203ea1 , the [[ states that this is a multidimensional array, and I stands for int (the type of the
        array). 74a14482 is the unsigned hexadecimal representation of the hash code of the array.
         */
        System.out.println(biArray);

        System.out.println();

        // Para imprimir el contenido del array correctamente existen varias maneras
        // Usamos dos ciclos for para recorrer filas y luego columnas
        for (int row = 0; row < biArray.length; row++) {
            for (int column = 0; column < biArray[row].length; column++) {
                // Accedemos a cada posición del array
                System.out.print(biArray[row][column] + " ");
            }
            System.out.println();
        }

        System.out.println();

        // Usamos dos ciclos for para recorrer filas y luego columnas
        for (int row = 0; row < biArray.length; row++) {
            for (int column = 0; column < biArray[row].length; column++) {
                // Accedemos a cada posición del array e imprimimos también la posición
                System.out.print("biArray[" + row + "]" + "[" + column + "] = " + biArray[row][column] + "   ");
            }
            System.out.println();
        }

        System.out.println();

        // Usamos Arrays.toString() en cada fila del array bidimensional
        for (int row = 0; row < biArray.length; row++) {
            System.out.println(Arrays.toString(biArray[row]));
        }

        System.out.println();

        // Definir un array con diferente tamaño de columnas para las filas
        int[][] difArray = new int[2][]; // Array con dos filas
        difArray [0] = new int[3]; // Crear 3 columnas para la fila 0
        difArray [1] = new int[2]; // Crear 2 columnas para la fila 1
        difArray[0][0] = 1;
        difArray[0][1] = 2;
        difArray[0][2] = 3;
        difArray[1][0] = 4;
        difArray[1][1] = 5;
        for (int row = 0; row < difArray.length; row++) {
            System.out.println(Arrays.toString(difArray[row]));
        }

        System.out.println();

        // Definir un array con valores que el usuario decida por consola
        System.out.println("Indique el número de filas del array bidimensional");
        int rowsFlexArray = Integer.parseInt(sc.nextLine());
        System.out.println("Indique el número de columnas del array bidimensional");
        int colsFlexArray = Integer.parseInt(sc.nextLine());
        // Inicialización del array bidimensional con los valores indicados
        int[][] flexArray = new int[rowsFlexArray][colsFlexArray];
        // Añadir los datos que el usuario indique por consola
        for (int row = 0; row < flexArray.length; row++) {
            for (int column = 0; column < flexArray[row].length; column++) {
                // Accedemos a cada posición del array  y pedimos el valor que desea guardar
                System.out.println("Ingrese el número de la posición [" + row + "][" + column + "]");
                flexArray[row][column] = Integer.parseInt(sc.nextLine());
            }
            System.out.println();
        }
        for (int row = 0; row < flexArray.length; row++) {
            System.out.println(Arrays.toString(flexArray[row]));
        }

        System.out.println();




    // Llave de cierre del psvm()
    }
// Llave de cierre de la clase
}

package co.edu.unac.poo1.collections.bidimensionalarray;

import java.util.Arrays;
import java.util.Scanner;

public class BidimensionalArrayInClass {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] biArray = {{1, 2}, {3, 4}};
        System.out.println("Hashcode = " + biArray);
        System.out.println("Fila 0 = " + Arrays.toString(biArray[0]));
        System.out.println("Fila 1 = " + Arrays.toString(biArray[1]));

        int[][] difArray = new int[2][]; // Array con dos filas
        difArray[0] = new int[3]; // Crear 3 columnas para la fila 0
        difArray[1] = new int[2]; // Crear 2 columnas para la fila 1
        difArray[0][0] = 1;
        difArray[0][1] = 2;
        difArray[0][2] = 3;
        difArray[1][0] = 4;
        difArray[1][1] = 5;
        System.out.println(Arrays.toString(difArray[0]));
        System.out.println(Arrays.toString(difArray[1]));

        // Definir un array con valores que el usuario decida por consola
        // Definir el número de filas
        System.out.println("Indique el número de filas del array bidimensional");
        int rowsFlexArray = Integer.parseInt(sc.nextLine());
        // Definir el número de columnas
        System.out.println("Indique el número de columnas del array bidimensional");
        int colsFlexArray = Integer.parseInt(sc.nextLine());
        // Inicialización del array bidimensional con los valores indicados
        int[][] flexArray = new int[rowsFlexArray][colsFlexArray];
        // Añadir los datos que el usuario indique por consola
        // for para iterar en las filas [2] -> 1
        for (int row = 0; row < flexArray.length; row++) {
            // for para iterar en las columnas [3] -> 0, 1, 2
            for (int column = 0; column < flexArray[row].length; column++) {
                // Accedemos a cada posición del array  y pedimos el valor que desea guardar
                System.out.println("Ingrese el número de la posición [" + row + "][" + column + "]");
                flexArray[row][column] = Integer.parseInt(sc.nextLine());
            }
            System.out.println();
        }
        // for para iterar las filas
        for (int row = 0; row < flexArray.length; row++) {
            // for para iterar las columnas
            for (int column = 0; column < flexArray[row].length; column++) {
                // Accedemos a cada posición del array e imprimimos el elemento en esa posición
                // Este es un print sin ln para que se imprima horizontalmente
                System.out.print("a[" + row + "]["+  column + "] = " + flexArray[row][column] + "   ");
            }
            // Cuando termine la fila deseo hacer un salto de línea
            System.out.println();
        }

        // for para iterar en las filas
        for (int row = 0; row < flexArray.length; row++) {
            // Arrays.toString() para cada fila
            System.out.println(Arrays.toString(flexArray[row]));
        }

    }
}

package co.edu.unac.poo1.collections.exercise.city;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;

public class Ciudad {

    // Atributos
    private String nombre;
    private String pais;
    private double[] temperatura;

    // Constructor por defecto
    public Ciudad() {

    }

    // Constructor con un parámetro
    public Ciudad(String nombre) {
        this.nombre = nombre;
    }

    public Ciudad(String nombre, String pais) {
        this.nombre = nombre;
        this.pais = pais;
    }

    // Constructor con todos los parámetros
    public Ciudad(String nombre, String pais, double[] temperatura) {
        this.nombre = nombre;
        this.pais = pais;
        this.temperatura = temperatura;
    }

    // Getter y setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public double[] getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double[] temperatura) {
        this.temperatura = temperatura;
    }

    // Ver información con toString()
    @Override
    public String toString() {
        return "Ciudad{" +
                "nombre='" + nombre + '\'' +
                ", pais='" + pais + '\'' +
                ", temperatura=" + Arrays.toString(temperatura) +
                '}';
    }

    // Identificar el día más caluroso
    public int diaMasCaluroso() {
        int dia = 0;
        double temperaturaMasAlta = temperatura[0]; // [28, 30.2, 27, 25, 27.3, 25, 22]
        // Recorrer el arreglo de temperatura
        for(int i = 0; i < temperatura.length; i++) { // temperatura.length = 7 --> 0, 6
            if(temperatura[i] > temperaturaMasAlta) {
                temperaturaMasAlta = temperatura[i]; // temperaturaMasAlta = 30.2
                dia = i; // dia =  1
            }
        }
        return dia;
    }

    // Identificar el día más frío
    public int diaMasFrio() {
        int dia = 0;
        double temperaturaMasBaja = temperatura[0];
        // Recorrer el arreglo de temperatura
        for(int i = 0; i < temperatura.length; i++) {
            if(temperatura[i] < temperaturaMasBaja) {
                temperaturaMasBaja = temperatura[i];
                dia = i;
            }
        }
        return dia;
    }

    // Promedio de temperaturas
    public String promedioTemperatura() {
        double sumaTemperaturasUltimos3Dias = 0;
        double promedio;
        DecimalFormatSymbols symbol = DecimalFormatSymbols.getInstance();
        symbol.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", symbol);
        for (int i = 4; i < temperatura.length; i++) {
            sumaTemperaturasUltimos3Dias += temperatura[i];
        }
        promedio = sumaTemperaturasUltimos3Dias/3;
        return df.format(promedio);

    }

    public double verTemperaturaDeUnDia(int dia) {
        return temperatura[dia];
    }

}
